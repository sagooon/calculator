``` Calculator ``` ```GUI Based``` ``` Author: sagooon ``` ```Script: Python```

![calculator](https://user-images.githubusercontent.com/74248485/126494856-5e33bf97-5be6-456a-a66f-814345ae9ed1.png)

``` Requirement: tkinter```   
```
pip install tk-tools
```

``` Calculate ```
```
python3 calculate.py
```



``` Source Code ```

![source code](https://user-images.githubusercontent.com/74248485/126496497-4ddfe488-b736-429a-b650-90ee79583b11.png)
